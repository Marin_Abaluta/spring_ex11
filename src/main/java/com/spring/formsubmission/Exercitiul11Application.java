package com.spring.formsubmission;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Exercitiul11Application {

	public static void main(String[] args) {
		SpringApplication.run(Exercitiul11Application.class, args);
	}
}
