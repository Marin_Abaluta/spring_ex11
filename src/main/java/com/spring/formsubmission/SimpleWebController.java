package com.spring.formsubmission;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class SimpleWebController {
	
	@RequestMapping(value = "/form", method = RequestMethod.GET)
	public String employeeForm(Model model) {
		
		model.addAttribute("employee", new Employee());
		return "form";
	}
	
	@RequestMapping(value = "/form", method = RequestMethod.POST)
	public String employeeSubmit(@ModelAttribute Employee employee) {
		
		return "result";
	}
	
		
	

}
